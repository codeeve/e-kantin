-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 02, 2019 at 10:42 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `project_midas`
--

-- --------------------------------------------------------

--
-- Table structure for table `ci_cookies`
--

CREATE TABLE `ci_cookies` (
  `id` int(11) NOT NULL,
  `cookie_id` varchar(255) DEFAULT NULL,
  `netid` varchar(255) DEFAULT NULL,
  `ip_address` varchar(255) DEFAULT NULL,
  `user_agent` varchar(255) DEFAULT NULL,
  `orig_page_requested` varchar(120) DEFAULT NULL,
  `php_session_id` varchar(40) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `user_data` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `config`
--

CREATE TABLE `config` (
  `config_name` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `form_answer`
--

CREATE TABLE `form_answer` (
  `_id` int(11) NOT NULL,
  `id_survey` int(11) NOT NULL,
  `id_question` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `answer` text NOT NULL,
  `files` varchar(255) DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `form_category`
--

CREATE TABLE `form_category` (
  `_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text,
  `image` varchar(255) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `form_category`
--

INSERT INTO `form_category` (`_id`, `name`, `description`, `image`, `created`) VALUES
(1, 'category 1', 'ini data testing isi category 1', NULL, '2019-08-07 02:43:58'),
(2, 'category 2', 'ini data testing category 2', NULL, '2019-08-07 02:43:58'),
(3, 'asdasdasd', 'adasdasd', NULL, '2019-08-07 03:08:58');

-- --------------------------------------------------------

--
-- Table structure for table `form_question`
--

CREATE TABLE `form_question` (
  `_id` int(11) NOT NULL,
  `id_category` int(11) NOT NULL,
  `id_question_type` int(11) NOT NULL,
  `question` text NOT NULL,
  `options` text,
  `mobile_visible` tinyint(1) NOT NULL DEFAULT '0',
  `image` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `form_question`
--

INSERT INTO `form_question` (`_id`, `id_category`, `id_question_type`, `question`, `options`, `mobile_visible`, `image`) VALUES
(1, 1, 1, 'pertanyaan test input manual?', NULL, 0, NULL),
(5, 1, 2, '<p><strong>Quill</strong> is a free, open source <a href=\"https://github.com/quilljs/quill/\" target=\"_blank\">WYSIWYG editor</a> built for the modern web. With its <a href=\"https://quilljs.com/docs/modules/\" target=\"_blank\">modular architecture</a> and expressive API, it is completely customizable to fit any need.</p>', 'quil, something, something 2', 1, NULL),
(8, 1, 3, '<p>contoh pertanyaan dengan image upload</p>', '', 1, NULL),
(9, 1, 4, '<p><strong>ini pertanyaan pake file upload</strong>.</p>', '', 1, NULL),
(11, 1, 1, '<p><strong>pertanyaan ini tidak tampil di mobile</strong></p>', '', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `form_question_type`
--

CREATE TABLE `form_question_type` (
  `_id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `description` text,
  `multiple_option` tinyint(1) NOT NULL DEFAULT '0',
  `is_file` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `form_question_type`
--

INSERT INTO `form_question_type` (`_id`, `name`, `description`, `multiple_option`, `is_file`) VALUES
(1, 'Normal Text', 'Pertanyaan text normal', 0, 0),
(2, 'Multiple Choice', 'Pertanyaan dengan pilihan jawaban', 0, 0),
(3, 'Image Upload', 'pertanyaan dengan image upload', 0, 1),
(4, 'File upload', 'pertanyaan dengan file upload', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `_id` int(11) NOT NULL,
  `username` varchar(32) NOT NULL,
  `password` varchar(50) NOT NULL,
  `level` int(2) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`_id`, `username`, `password`, `level`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 99),
(2, 'saya', 'd5c39a1d9aced393f835f334a1fb9206', 2),
(3, 'kepsek', '8561863b55faf85b9ad67c52b3b851ac', 3),
(6, 'tata_usaha', '5f7d6e9e00a362d45528a43f03ec37c9', 4),
(7, 'adminasdas', '21232f297a57a5a743894a0e4a801fc3', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_information`
--

CREATE TABLE `user_information` (
  `_id` int(11) NOT NULL,
  `_id_user` int(11) NOT NULL,
  `name` varchar(120) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ci_cookies`
--
ALTER TABLE `ci_cookies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD PRIMARY KEY (`session_id`),
  ADD KEY `last_activity_idx` (`last_activity`);

--
-- Indexes for table `config`
--
ALTER TABLE `config`
  ADD PRIMARY KEY (`config_name`);

--
-- Indexes for table `form_answer`
--
ALTER TABLE `form_answer`
  ADD PRIMARY KEY (`_id`);

--
-- Indexes for table `form_category`
--
ALTER TABLE `form_category`
  ADD PRIMARY KEY (`_id`);

--
-- Indexes for table `form_question`
--
ALTER TABLE `form_question`
  ADD PRIMARY KEY (`_id`);

--
-- Indexes for table `form_question_type`
--
ALTER TABLE `form_question_type`
  ADD PRIMARY KEY (`_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`_id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `user_information`
--
ALTER TABLE `user_information`
  ADD PRIMARY KEY (`_id`),
  ADD KEY `_id_user` (`_id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ci_cookies`
--
ALTER TABLE `ci_cookies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `form_answer`
--
ALTER TABLE `form_answer`
  MODIFY `_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `form_category`
--
ALTER TABLE `form_category`
  MODIFY `_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `form_question`
--
ALTER TABLE `form_question`
  MODIFY `_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `form_question_type`
--
ALTER TABLE `form_question_type`
  MODIFY `_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `user_information`
--
ALTER TABLE `user_information`
  MODIFY `_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `user_information`
--
ALTER TABLE `user_information`
  ADD CONSTRAINT `user_information_ibfk_1` FOREIGN KEY (`_id_user`) REFERENCES `user` (`_id`) ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
