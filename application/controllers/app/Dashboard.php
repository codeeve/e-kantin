<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	    /**
    * Responsable for auto load the model
    * @return void
    */
    public function __construct()
    {
        parent::__construct();
        //cek admin permission
		/*if ($this->session->userdata('user_level')>=99 || $this->session->userdata('user_level')=="" || $this->session->userdata('user_level')<=0 ){
            redirect('auth');
        }*/
    }

	public function index()
	{
		//load the view
        $data['main_content'] = 'app/dashboard';
        $this->load->view('_includes/user/template', $data);

    }
    
}
