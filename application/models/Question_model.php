<?php

class Question_model extends CI_Model {

	function count()
    {
		$this->db->select('*');
		$this->db->from('form_category');
		$query = $this->db->get();
		return $query->num_rows();
    }

	public function get_question_by_category($category_id)
    {

		$this->db->select('form_question._id as _id, form_question.mobile_visible, form_question.question,form_question.options, form_question_type.name as type,form_question_type._id as type_id, form_question_type.multiple_option, form_question_type.is_file');
		$this->db->from('form_question');
		$this->db->where('id_category', $category_id);
		$this->db->join('form_question_type','form_question.id_question_type = form_question_type._id','left');

		$query = $this->db->get();

		return $query->result_array();
	}
	
	function get_by_question_id($question_id)
    {

		$this->db->select('*');
		$this->db->from('form_question');
		$this->db->where('_id', $question_id);
		$query = $this->db->get();

		return $query->row_array();
    }

	function delete($id){
		$this->db->where('_id', $id);
		$this->db->delete('form_question');
	}

	function update($id, $data){
		$this->db->where('_id', $id);
		$this->db->update('form_question', $data);
	}

	function create($data)
	{
		$insert = $this->db->insert('form_question', $data);
		return $insert;

	}

	public function get_question_by_user($category_id, $user_id)
    {

		$this->db->select('*');
		$this->db->from('form_question');
		$query = $this->db->get();

		return $query->result_array();
	}

	public function get_question_type_all()
    {

		$this->db->select('*');
		$this->db->from('form_question_type');
		$query = $this->db->get();

		return $query->result_array();
	}
}
