<?php

class Users_model extends CI_Model {

    /**
    * Validate the login's data with the database
    * @param string $user_name
    * @param string $password
    * @return void
    */
	function validate($user_name, $password)
	{
		$this->db->where('username', $user_name);
		$this->db->where('password', $password);
		$query = $this->db->get('user');

		// echo var_dump($query);

		if($query->num_rows() == 1)
		{
			return true;
		}
	}

	function get_userid($user_name)
	{
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where('username', $user_name);
		$query = $this->db->get();

		return $query->row('id');
	}

	function get_userlevel($user_name)
	{
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where('username', $user_name);
		$query = $this->db->get();

		return $query->row('level');
	}


    /**
    * Serialize the session data stored in the database,
    * store it in a new array and return it to the controller
    * @return array
    */
	function get_db_session_data()
	{
		$query = $this->db->select('user_data')->get('ci_sessions');
		$user = array(); /* array to store the user data we fetch */
		foreach ($query->result() as $row)
		{
		    $udata = unserialize($row->user_data);
		    /* put data in array using username as key */
		    $user['user_name'] = $udata['user_name'];
		    $user['is_logged_in'] = $udata['is_logged_in'];
		}
		return $user;
	}

    /**
    * Store the new user's data into the database
    * @return boolean - check the insert
    */
	function create_member()
	{

		$this->db->where('user_name', $this->input->post('username'));
		$query = $this->db->get('user');

        if($query->num_rows > 0){
        	echo '<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>';
			  echo "Username already taken";
			echo '</strong></div>';
		}else{

			$new_member_insert_data = array(
				'first_name' => $this->input->post('first_name'),
				'last_name' => $this->input->post('last_name'),
				'email_addres' => $this->input->post('email_address'),
				'user_name' => $this->input->post('username'),
				'pass_word' => md5($this->input->post('password'))
			);
			$insert = $this->db->insert('user', $new_member_insert_data);
		    return $insert;
		}

	}//create_member

	function create_member_api()
	{

		$this->db->where('user_name', $this->input->post('username'));
		$query = $this->db->get('user');

        if($query->num_rows > 0){
        	return false;
		}else{

			$new_member_insert_data = array(
				'username' => $this->input->post('username'),
				'password' => md5($this->input->post('password'))
			);
			$insert = $this->db->insert('user', $new_member_insert_data);
		    return $insert;
		}

	}//create_member

	function count_user()
    {
		$this->db->select('*');
		$this->db->from('user');
		$query = $this->db->get();
		return $query->num_rows();
    }

	public function get_user($limit_start, $limit_end)
    {

		$this->db->select('*');
		$this->db->from('user');
		$this->db->limit($limit_start, $limit_end);
		$query = $this->db->get();

		return $query->result_array();
    }

	function get_user_by_id($id)
    {

		$this->db->select('*');
		$this->db->from('user');
		$this->db->where('id', $id);
		$query = $this->db->get();

		return $query->result_array();
    }

	function delete_user($id){
		$this->db->where('id', $id);
		$this->db->delete('user');
	}
}
