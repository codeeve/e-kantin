<?php

class Category_model extends CI_Model {

	function count()
    {
		$this->db->select('*');
		$this->db->from('form_category');
		$query = $this->db->get();
		return $query->num_rows();
    }

	public function get_all()
    {

		$this->db->select('*');
		$this->db->from('form_category');
		$query = $this->db->get();

		return $query->result_array();
	}
	
	public function get($limit_start, $limit_end)
    {

		$this->db->select('*');
		$this->db->from('form_category');
		$this->db->limit($limit_start, $limit_end);
		$query = $this->db->get();

		return $query->result_array();
    }

	function get_by_id($id)
    {

		$this->db->select('*');
		$this->db->from('form_category');
		$this->db->where('_id', $id);
		$query = $this->db->get();

		return $query->row_array();
    }

	function delete($id){
		$this->db->where('_id', $id);
		$this->db->delete('form_category');
	}

	function create($data)
	{
		$insert = $this->db->insert('form_category', $data);
		return $insert;

	}
}
