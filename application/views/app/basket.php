

    <div class="content content-fixed">
      <div class="container pd-x-0 pd-lg-x-10 pd-xl-x-0">
        <div class="row row-xs">
          <div class="col-sm-12 col-lg-12">
            <div class="card card-body">
              <h6 class="tx-uppercase tx-11 tx-spacing-1 tx-color-02 tx-semibold mg-b-8">Hatsune Miku</h6>
              <div class="d-flex d-lg-block d-xl-flex align-items-end">
                <h3 class="tx-normal tx-rubik mg-b-0 mg-r-5 lh-1">Rp 500.000,-</h3>
              </div>
              <div class="chart-three">
                  <div id="flotChart3" class="flot-chart ht-30"></div>
                </div><!-- chart-three -->
            </div>
          </div><!-- col -->

          <div class="col-md-6 col-xl-4 mg-t-10">

            <div class="card ht-100p">
              <div class="card-header d-flex align-items-center justify-content-between">
                <h6 class="mg-b-0">Order Summary</h6>
              </div>
              <ul class="list-unstyled media-list tx-12 tx-sm-13 mg-b-0">
                
                <li class="media bg-ui-01 pd-y-10 pd-x-15">
                  <div class="avatar"><img src="https://via.placeholder.com/500" class="rounded-circle" alt=""></div>
                  <div class="media-body mg-l-15">
                    <h6 class="tx-13 mg-b-2">Nama Makanan</h6>
                    <span class="d-block tx-color-03">Rp 5.000,-</span>
                  </div><!-- media-body -->
                  <a href="" class="btn btn-white rounded-circle btn-icon mg-l-15"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-plus"><line x1="12" y1="5" x2="12" y2="19"></line><line x1="5" y1="12" x2="19" y2="12"></line></svg></a>
                </li><!-- media -->

                <li class="media bg-ui-01 pd-y-10 pd-x-15">
                  <div class="avatar"><img src="https://via.placeholder.com/500" class="rounded-circle" alt=""></div>
                  <div class="media-body mg-l-15">
                    <h6 class="tx-13 mg-b-2">Nama Makanan</h6>
                    <span class="d-block tx-color-03">Rp 5.000,-</span>
                  </div><!-- media-body -->
                  <a href="" class="btn btn-white rounded-circle btn-icon mg-l-15"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-plus"><line x1="12" y1="5" x2="12" y2="19"></line><line x1="5" y1="12" x2="19" y2="12"></line></svg></a>
                </li><!-- media -->

                <li class="media bg-ui-01 pd-y-10 pd-x-15">
                  <div class="avatar"><img src="https://via.placeholder.com/500" class="rounded-circle" alt=""></div>
                  <div class="media-body mg-l-15">
                    <h6 class="tx-13 mg-b-2">Nama Makanan</h6>
                    <span class="d-block tx-color-03">Rp 5.000,-</span>
                  </div><!-- media-body -->
                  <a href="" class="btn btn-white rounded-circle btn-icon mg-l-15"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-plus"><line x1="12" y1="5" x2="12" y2="19"></line><line x1="5" y1="12" x2="19" y2="12"></line></svg></a>
                </li><!-- media -->

                <li class="media bg-ui-01 pd-y-10 pd-x-15">
                  <div class="avatar"><img src="https://via.placeholder.com/500" class="rounded-circle" alt=""></div>
                  <div class="media-body mg-l-15">
                    <h6 class="tx-13 mg-b-2">Nama Makanan</h6>
                    <span class="d-block tx-color-03">Rp 5.000,-</span>
                  </div><!-- media-body -->
                  <a href="" class="btn btn-white rounded-circle btn-icon mg-l-15"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-plus"><line x1="12" y1="5" x2="12" y2="19"></line><line x1="5" y1="12" x2="19" y2="12"></line></svg></a>
                </li><!-- media -->

                <li class="media bg-ui-01 pd-y-10 pd-x-15">
                  <div class="avatar"><img src="https://via.placeholder.com/500" class="rounded-circle" alt=""></div>
                  <div class="media-body mg-l-15">
                    <h6 class="tx-13 mg-b-2">Nama Makanan</h6>
                    <span class="d-block tx-color-03">Rp 5.000,-</span>
                  </div><!-- media-body -->
                  <a href="" class="btn btn-white rounded-circle btn-icon mg-l-15"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-plus"><line x1="12" y1="5" x2="12" y2="19"></line><line x1="5" y1="12" x2="19" y2="12"></line></svg></a>
                </li><!-- media -->
              </ul>
             </div><!-- card -->
          </div>
          <div class="col-lg-12 col-xl-8 mg-t-10">
            <div class="card card-body ht-lg-100">
              <div class="media">
                <div class="media-body">
                  <h6 class="mg-b-10">Subtotal</h6>
                  <p class="tx-color-01 tx-right tx-bold tx-18 mg-b-0">Rp 500.000,-</p>
                </div>
              </div><!-- media -->
            </div>
          </div>

          <div class="col-lg-12 col-xl-8 mg-t-10">
          <button type="button" class="btn btn-primary btn-block">Place Order</button>
          </div>

          
        </div><!-- row -->
        
      </div><!-- container -->
    </div><!-- content -->

 