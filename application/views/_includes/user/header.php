<!DOCTYPE html>
<html lang="en">
  <head>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    

    <title>eKantin Unhas</title>

    <!-- vendor css -->
    <link href="<?php echo base_url();?>lib/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>lib/ionicons/css/ionicons.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>lib/jqvmap/jqvmap.min.css" rel="stylesheet">

    <!-- DashForge CSS -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/dashforge.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/dashforge.dashboard.css">
  </head>
  <body class="page-profile">

    <header class="navbar navbar-header navbar-header-fixed">
      <a href="" class="burger-menu"><i data-feather="arrow-left"></i></a>
      <div class="navbar-brand">
        <a href="<?php echo base_url();?>/app" class="df-logo">e<span>Kantin</span></a>
      </div><!-- navbar-brand -->
    </header><!-- navbar -->